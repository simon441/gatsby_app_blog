import React from 'react'
import Layout from '../components/layout'

const ServicesPage = () => {
  return (
    <Layout>
     <h1>Our Services</h1>
     <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Consectetur libero id faucibus nisl tincidunt eget nullam non nisi. Nulla facilisi nullam vehicula ipsum a arcu cursus. Dui sapien eget mi proin sed libero enim. In fermentum et sollicitudin ac orci phasellus egestas tellus. </p>
     <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Consectetur libero id faucibus nisl tincidunt eget nullam non nisi. Nulla facilisi nullam vehicula ipsum a arcu cursus. Dui sapien eget mi proin sed libero enim. In fermentum et sollicitudin ac orci phasellus egestas tellus. </p>
    </Layout>
  )
}

export default ServicesPage
