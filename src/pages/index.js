import React from "react"
// import { Link } from "gatsby"

import Layout from "../components/layout"

const IndexPage = () => (
  <Layout>

    <h1>Welcome my website.</h1>
    <p>This is a sample site for the Gatsby crash course.</p>
    {/* <div style={{ maxWidth: `300px`, marginBottom: `1.45rem` }}>
      <Image />
    </div> */}
  </Layout>
)

export default IndexPage
